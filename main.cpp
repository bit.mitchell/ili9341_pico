
#include "pico/stdlib.h"
#include "hardware/spi.h"

#define TFT_WIDTH  320
#define TFT_HEIGHT 240

// Color definitions for backwards compatibility with old sketches
// use colour definitions like TFT_BLACK to make sketches more portable
#define ILI9341_BLACK       0x0000      /*   0,   0,   0 */
#define ILI9341_NAVY        0x000F      /*   0,   0, 128 */
#define ILI9341_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define ILI9341_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define ILI9341_MAROON      0x7800      /* 128,   0,   0 */
#define ILI9341_PURPLE      0x780F      /* 128,   0, 128 */
#define ILI9341_OLIVE       0x7BE0      /* 128, 128,   0 */
#define ILI9341_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define ILI9341_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define ILI9341_BLUE        0x001F      /*   0,   0, 255 */
#define ILI9341_GREEN       0x07E0      /*   0, 255,   0 */
#define ILI9341_CYAN        0x07FF      /*   0, 255, 255 */
#define ILI9341_RED         0xF800      /* 255,   0,   0 */
#define ILI9341_MAGENTA     0xF81F      /* 255,   0, 255 */
#define ILI9341_YELLOW      0xFFE0      /* 255, 255,   0 */
#define ILI9341_WHITE       0xFFFF      /* 255, 255, 255 */
#define ILI9341_ORANGE      0xFD20      /* 255, 165,   0 */
#define ILI9341_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define ILI9341_PINK        0xF81F

#define RGB(r, g, b)        (((uint8_t)((r) & 0xF8) << 8) | ((uint8_t)((g) & 0xFC) << 3) | ((uint8_t)((b) & 0xF8) >> 3))


#define TFT_NOP     0x00
#define TFT_SWRST   0x01

#define TFT_CASET   0x2A
#define TFT_PASET   0x2B
#define TFT_RAMWR   0x2C

#define TFT_RAMRD   0x2E
#define TFT_IDXRD   0xDD // ILI9341 only, indexed control register read

#define TFT_MADCTL  0x36
#define TFT_MAD_MY  0x80
#define TFT_MAD_MX  0x40
#define TFT_MAD_MV  0x20
#define TFT_MAD_ML  0x10
#define TFT_MAD_BGR 0x08
#define TFT_MAD_MH  0x04
#define TFT_MAD_RGB 0x00

#define TFT_INVOFF  0x20
#define TFT_INVON   0x21

// All ILI9341 specific commands some are used by init()
#define ILI9341_NOP     0x00
#define ILI9341_SWRESET 0x01
#define ILI9341_RDDID   0x04
#define ILI9341_RDDST   0x09

#define ILI9341_SLPIN   0x10
#define ILI9341_SLPOUT  0x11
#define ILI9341_PTLON   0x12
#define ILI9341_NORON   0x13

#define ILI9341_RDMODE  0x0A
#define ILI9341_RDMADCTL  0x0B
#define ILI9341_RDPIXFMT  0x0C
#define ILI9341_RDIMGFMT  0x0A
#define ILI9341_RDSELFDIAG  0x0F

#define ILI9341_INVOFF  0x20
#define ILI9341_INVON   0x21
#define ILI9341_GAMMASET 0x26
#define ILI9341_DISPOFF 0x28
#define ILI9341_DISPON  0x29

#define ILI9341_CASET   0x2A
#define ILI9341_PASET   0x2B
#define ILI9341_RAMWR   0x2C
#define ILI9341_RAMRD   0x2E

#define ILI9341_PTLAR   0x30
#define ILI9341_VSCRDEF 0x33
#define ILI9341_MADCTL  0x36
#define ILI9341_VSCRSADD 0x37
#define ILI9341_PIXFMT  0x3A

#define ILI9341_WRDISBV  0x51
#define ILI9341_RDDISBV  0x52
#define ILI9341_WRCTRLD  0x53

#define ILI9341_FRMCTR1 0xB1
#define ILI9341_FRMCTR2 0xB2
#define ILI9341_FRMCTR3 0xB3
#define ILI9341_INVCTR  0xB4
#define ILI9341_DFUNCTR 0xB6

#define ILI9341_PWCTR1  0xC0
#define ILI9341_PWCTR2  0xC1
#define ILI9341_PWCTR3  0xC2
#define ILI9341_PWCTR4  0xC3
#define ILI9341_PWCTR5  0xC4
#define ILI9341_VMCTR1  0xC5
#define ILI9341_VMCTR2  0xC7

#define ILI9341_RDID4   0xD3
#define ILI9341_RDINDEX 0xD9
#define ILI9341_RDID1   0xDA
#define ILI9341_RDID2   0xDB
#define ILI9341_RDID3   0xDC
#define ILI9341_RDIDX   0xDD // TBC

#define ILI9341_GMCTRP1 0xE0
#define ILI9341_GMCTRN1 0xE1

#define ILI9341_MADCTL_MY  0x80
#define ILI9341_MADCTL_MX  0x40
#define ILI9341_MADCTL_MV  0x20
#define ILI9341_MADCTL_ML  0x10
#define ILI9341_MADCTL_RGB 0x00
#define ILI9341_MADCTL_BGR 0x08
#define ILI9341_MADCTL_MH  0x04

class ILI9341
{
public:
  ILI9341(spi_inst_t *spi,
          uint SCK,
          uint MOSI,
          uint MISO,
          uint CS,
          uint LED,
          uint DC,
          uint RST) :
  spi(spi),
  SCK(SCK),
  MOSI(MOSI),
  MISO(MISO),
  CS(CS),
  LED(LED),
  DC(DC),
  RST(RST) {
    Reset();
  }

  // Reset the display driver and I/O pins
  void Reset()
  {
    spi_init(spi, 1000000);
    spi_set_format(spi, 8, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);

    gpio_set_function(SCK, GPIO_FUNC_SPI);
    gpio_set_function(MOSI, GPIO_FUNC_SPI);
    gpio_set_function(MISO, GPIO_FUNC_SPI);

    gpio_init(CS);
    gpio_set_dir(CS, GPIO_OUT);
    gpio_put(CS, 1);

    gpio_init(LED);
    gpio_set_dir(LED, GPIO_OUT);
    gpio_put(LED, 0);

    gpio_init(DC);
    gpio_set_dir(DC, GPIO_OUT);
    gpio_put(DC, 0);


    gpio_init(RST);
    gpio_set_dir(RST, GPIO_OUT);

    // reset the device
    gpio_put(RST, 0);
    sleep_us(100);
    gpio_put(RST, 1);

    // Command(0xEF);
    // Data(0x03);
    // Data(0x80);
    // Data(0x02);

    Command(0xCF);
    Data(0x00);
    Data(0XC1);
    Data(0X30);

    Command(0xED);
    Data(0x64);
    Data(0x03);
    Data(0X12);
    Data(0X81);

    Command(0xE8);
    Data(0x85);
    Data(0x00);
    Data(0x78);

    Command(0xCB);
    Data(0x39);
    Data(0x2C);
    Data(0x00);
    Data(0x34);
    Data(0x02);

    Command(0xF7);
    Data(0x20);

    Command(0xEA);
    Data(0x00);
    Data(0x00);

    Command(ILI9341_PWCTR1);    //Power control
    Data(0x10);   //VRH[5:0]

    Command(ILI9341_PWCTR2);    //Power control
    Data(0x00);   //SAP[2:0];BT[3:0]

    Command(ILI9341_VMCTR1);    //VCM control
    Data(0x30);
    Data(0x30);

    Command(ILI9341_VMCTR2);    //VCM control2
    Data(0xB7);  //--

    Command(ILI9341_MADCTL);    // Memory Access Control
    Data(TFT_MAD_MX | TFT_MAD_MY | TFT_MAD_MV | TFT_MAD_BGR); // Rotation 0 (portrait mode)

    Command(ILI9341_CASET);
    Data16(0);
    Data16(TFT_WIDTH - 1);

    Command(ILI9341_PASET);
    Data16(0);
    Data16(TFT_HEIGHT - 1);

    Command(ILI9341_PIXFMT);
    Data(0x55);

    Command(ILI9341_FRMCTR1);
    Data(0x00);
    Data(0x13); // 0x18 79Hz, 0x1B default 70Hz, 0x13 100Hz

    Command(ILI9341_DFUNCTR);    // Display Function Control
    Data(0x08);
    Data(0x82);
    Data(0x27);

    Command(0xF2);    // 3Gamma Function Disable
    Data(0x00);

    Command(ILI9341_GAMMASET);    //Gamma curve selected
    Data(0x01);

    Command(0xE0); //Set Gamma
    Data(0x0F);
    Data(0x2A);
    Data(0x28);
    Data(0x08);
    Data(0x0E);
    Data(0x08);
    Data(0x54);
    Data(0xA9);
    Data(0x43);
    Data(0x0A);
    Data(0x0F);
    Data(0x00);
    Data(0x00);
    Data(0x00);
    Data(0x00);

    Command(0XE1); //Set Gamma
    Data(0x00);
    Data(0x15);
    Data(0x17);
    Data(0x07);
    Data(0x11);
    Data(0x06);
    Data(0x2B);
    Data(0x56);
    Data(0x3C);
    Data(0x05);
    Data(0x10);
    Data(0x0F);
    Data(0x3F);
    Data(0x3F);
    Data(0x0F);

    Command(ILI9341_SLPOUT);    //Exit Sleep

    gpio_put(CS, 1);
    sleep_ms(120);
    gpio_put(CS, 0);

    Command(ILI9341_DISPON);    //Display on
  }

  void Backlight(bool enable)
  {
    gpio_put(LED, enable);
  }

  void Draw(const uint16_t* data, uint pixels)
  {
    Command(0x2C);

    spi_set_format(spi, 16, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);

    gpio_put(DC, 1);
    gpio_put(CS, 0);
    spi_write16_blocking(spi, data, pixels);
    gpio_put(CS, 1);

    spi_set_format(spi, 8, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);
  }

  void Clear(uint16_t color)
  {
    Command(0x2C);

    spi_set_format(spi, 16, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);

    gpio_put(DC, 1);
    gpio_put(CS, 0);

    uint len = TFT_WIDTH * TFT_HEIGHT;
    while (len--)
    {
      spi_write16_blocking(spi, &color, 1);
    }

    gpio_put(CS, 1);

    spi_set_format(spi, 8, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);
  }

private:

  void Command(uint8_t command)
  {
    gpio_put(DC, 0);
    gpio_put(CS, 0);
    spi_write_blocking(spi, &command, 1);
    gpio_put(CS, 1);
  }

  void Data(uint8_t data)
  {
    gpio_put(DC, 1);
    gpio_put(CS, 0);
    spi_write_blocking(spi, &data, 1);
    gpio_put(CS, 1);
  }

  void Data16(uint16_t data)
  {
    data = Swap16(data);

    gpio_put(DC, 1);
    gpio_put(CS, 0);
    spi_write_blocking(spi, (uint8_t*)&data, 2);
    gpio_put(CS, 1);
  }

  uint16_t Swap16(uint16_t num)
  {
    return (num>>8) | (num<<8);
  }

  spi_inst_t *spi;
  uint SCK;
  uint MOSI;
  uint MISO;
  uint CS;
  uint LED;
  uint DC;
  uint RST;
};

int main() {
  ILI9341 screen(spi0, 18, 19, 16, 17, 20, 21, 22);

  uint16_t pixels[TFT_WIDTH * TFT_HEIGHT];
  for (int y = 0; y < TFT_HEIGHT; ++y) {
    for (int x = 0; x < TFT_WIDTH; x++)
    {
      if (y < 80)
      {
        pixels[x + y * TFT_WIDTH] = RGB(x, 0, 0);
      }
      else if (y < 160)
      {
        pixels[x + y * TFT_WIDTH] = RGB(0, x, 0);
      }
      else
      {
        pixels[x + y * TFT_WIDTH] = RGB(0, 0, x);
      }
    }
  }

  screen.Backlight(true);
  screen.Draw(pixels, TFT_WIDTH * TFT_HEIGHT);
}
